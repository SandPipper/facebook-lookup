def for_all_methods(decorator):
    def decorate(cls):
        for attr in cls.__dict__: # there's propably a better way to do this
            if callable(getattr(cls, attr)):
                setattr(cls, attr, decorator(getattr(cls, attr)))
        return cls
    return decorate


def static(cls):
    def decorate():
        return for_all_methods(classmethod)(cls)
    return decorate()

from .fb_tools import LabelMaker
