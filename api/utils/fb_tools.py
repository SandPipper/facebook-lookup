import json
import copy
import time

import requests

from django.conf import settings
from api.utils import static


class FBError(Exception):
    pass


@static
class LabelMaker:
    response_template = {'status': 'ok', 'error': {}}

    def __perform_batch_request(cls, requests_list, include_headers=False):
        responses_list = []
        while True:
            batch = requests_list[len(responses_list):len(responses_list) + settings.FB_GRAPH_API_MAX_BATCH_SIZE]
            if not batch:
                break

            url = '{}/v{}?include_headers={}'.format(settings.FB_GRAPH_API_URL, settings.FB_GRAPH_API_VERSION,
                                                     'true' if include_headers else 'false')
            payload = {'access_token': settings.FB_GRAPH_API_ACCESS_TOKEN, 'batch': batch}

            for response in requests.post(url=url, json=payload).json():
                try:
                    if response is not None:
                        response['body'] = json.loads(response['body'])
                        responses_list.append(response)
                    else:
                        break

                except Exception:
                    raise FBError(response)

            time.sleep(settings.FB_GRAPH_API_REQUEST_TIMEOUT / 1000)

        return responses_list

    def __get_label_id_by_name(cls, label_name):
        url = '{}/v{}/me/custom_labels'.format(settings.FB_GRAPH_API_URL, settings.FB_GRAPH_API_VERSION)
        params = {'access_token': settings.FB_GRAPH_API_ACCESS_TOKEN, 'fields': 'name'}
        while True:
            resp = requests.get(url=url, params=params).json()
            time.sleep(settings.FB_GRAPH_API_REQUEST_TIMEOUT / 1000)
            for label in resp['data']:
                if label['name'] == label_name:
                    return label['id']

            try:
                params['after'] = resp['paging']['cursors']['after']
            except KeyError:
                return None

    def __create_label(cls, label_name):
        url = '{}/v{}/me/custom_labels'.format(settings.FB_GRAPH_API_URL, settings.FB_GRAPH_API_VERSION)
        payload = {'access_token': settings.FB_GRAPH_API_ACCESS_TOKEN, 'name': label_name}
        resp = requests.post(url=url, json=payload).json()
        time.sleep(settings.FB_GRAPH_API_REQUEST_TIMEOUT / 1000)

        try:
            return resp['id']
        except KeyError:
            raise FBError(resp['error'])

    def add_label_to_psids(cls, label_name, psids_list):
        label_id = cls.__get_label_id_by_name(label_name) or cls.__create_label(label_name)
        requests_list = [
            {
                'method': 'POST',
                'relative_url': '/{}/label'.format(label_id),
                'body': 'user={}'.format(psid)
            } for psid in psids_list
        ]
        responses_list = cls.__perform_batch_request(requests_list)
        result = {}

        for psid, resp in zip(psids_list, responses_list):
            copy_response_template = copy.deepcopy(cls.response_template)
            if not resp['code'] == 200:
                copy_response_template['error'] = resp['body']['error']
                copy_response_template['status'] = 'fail'
            result[psid] = copy_response_template

        return result
