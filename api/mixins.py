import json
import csv

from django.conf import settings

from rest_framework.response import Response
from rest_framework import status

from api.abstract.mixins import BaseMixin


class BaseValidationMixin(BaseMixin):
    def validate_request(self, request):
        self.errors = []

    def validate_users_amount(self):
        pass

    def add_error(self, error,):
        self.errors.append(error)
        return self.handle_errors()

    def handle_errors(self):
        if len(self.errors) and not self.failed_response:
            self.failed_response = Response(
                data=self.fail_response,
                status=status.HTTP_400_BAD_REQUEST
            )
            return self.failed_response


class URLParametersValidationMixin(BaseValidationMixin):
    def validate_request(self, request):
        super().validate_request(request)

        self.label_name = request.GET.get('label', None)
        self.psids_list = request.GET.get('psids', None)

        if not self.label_name:
            self.add_error('Label is not specified')

        if not self.psids_list:
            self.add_error('PSIDs is not specified.')

        if self.failed_response:
            return

        self.validate_users_amount()

    def validate_users_amount(self):
        _psids_list = self.psids_list.split(',')

        if len(_psids_list) > settings.API_MAX_USERS_PER_REQUEST:
            self.add_error('Too many PSIDs in request')

        if self.failed_response:
            return


class FileValidationMixin(BaseValidationMixin):
    def validate_request(self, request):
        super().validate_request(request)

        self.file = request.FILES.get('file', None)

        if not self.file:
            self.add_error('File is required')

        if self.file.name == '':
            self.add_error('Filename is required')

        if self.failed_response:
            return

        if self.is_json:
            self.validate_json(request)
        elif self.is_csv:
            self.validate_csv(request)

    def validate_json(self, request):
        file_data = self.file.read().decode('utf-8')
        try:
            prepared_data = json.loads(file_data)
            self.psids_list = prepared_data['psids']
            self.label_name = prepared_data['label']
        except (json.JSONDecodeError, AttributeError, ValueError):
            self.add_error('Uploaded file is not a valid JSON file')
            if self.failed_response:
                return
        self.validate_users_amount()

    def validate_csv(self, request):
        decoded_file = self.file.read().decode('utf-8').splitlines()
        reader = csv.DictReader(decoded_file)
        result = []
        label = None
        for row in reader:
            label = [*row][0]
            result.append(row[label])

        self.label_name = label
        if not self.label_name:
            return self.add_error('Label is not specified')

        self.psids_list = result
        self.label_name = label
        self.validate_users_amount()

    def validate_users_amount(self):
        _psids_list = self.psids_list

        if len(_psids_list) > settings.API_MAX_USERS_PER_REQUEST:
            self.add_error('Too many PSIDs in request')

        if self.failed_response:
            return
