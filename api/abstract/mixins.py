import copy
from django.conf import settings
from api.utils import LabelMaker


class BaseMixin:
    data = None
    errors = None

    is_json = False
    is_csv = False
    file = None

    label_name = ''
    psids_list = None

    success_status = 'ok'
    fail_status = 'fail'
    response_template = copy.deepcopy(settings.RESPONSE_TEMPLATE)

    failed_response = None

    @property
    def is_file(self):
        return self.is_json or self.is_csv

    @property
    def fail_response(self):
        self.response_template['status'] = self.fail_status
        self.response_template['errors'] = self.errors
        return self.response_template

    @property
    def success_response(self):
        self.response_template['status'] = self.success_status
        if type(self.psids_list) is list:
            _psids_list = self.psids_list
        else:
            _psids_list = self.psids_list.split(',')

        self.data = LabelMaker.add_label_to_psids(
            label_name=self.label_name,
            psids_list=_psids_list
        )
        self.response_template['data'] = self.data
        return self.response_template
