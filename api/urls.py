from django.conf.urls import url
from .views import FacebookLookupView, JSONFacebookLookupView, CSVFacebookLookupView

urlpatterns = [
    url(r'^$', FacebookLookupView.as_view()),
    url(r'^json/$', JSONFacebookLookupView.as_view()),
    url(r'^csv/$', CSVFacebookLookupView.as_view())
]