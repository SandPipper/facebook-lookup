#!/bin/bash
set -e

./manage.py makemigrations --no-input
./manage.py migrate --no-input
./manage.py collectstatic --no-input
gunicorn -w 4 facebook_lookup.wsgi:application --bind=0.0.0.0:8000 --log-level debug --reload
